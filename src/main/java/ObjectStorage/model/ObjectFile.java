package ObjectStorage.model;


import org.bson.types.ObjectId;
import java.util.ArrayList;
import java.util.HashMap;

public class ObjectFile {

    private ObjectId id;
    private String objectFileName;
    private HashMap<Integer, ArrayList> objectFileParts;
    private String objectUUID;
    private String ticket;
    private long modified;
    private long created;
    private long totalContentLength;
    private HashMap<String, String> metadata;

    public ObjectFile(String objectFileName, String objectUUID){
        this.objectFileName = objectFileName;
        this.objectUUID = objectUUID;
        this.ticket = "incomplete";
        this.objectFileParts = new HashMap<>();
        this.totalContentLength = 0;
        this.metadata = new HashMap<>();

    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getObjectFileName() {
        return objectFileName;
    }

    public void setObjectFileName(String objectFileName) {
        this.objectFileName = objectFileName;
    }



    public String getObjectUUID() {
        return objectUUID;
    }

    public void setObjectUUID(String objectUUID) {
        this.objectUUID = objectUUID;
    }

    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    public long getModified() {
        return modified;
    }

    public void setModified(long modified) {
        this.modified = modified;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public HashMap<Integer, ArrayList> getObjectFileParts() {
        return objectFileParts;
    }

    public void setObjectFileParts(HashMap<Integer, ArrayList> objectFileParts) {
        this.objectFileParts = objectFileParts;
    }

    public long getTotalContentLength() {
        return totalContentLength;
    }

    public void setTotalContentLength(long totalContentLength) {
        this.totalContentLength = totalContentLength;
    }

    public HashMap<String, String> getMetadata() {
        return metadata;
    }

    public void setMetadata(HashMap<String, String> metadata) {
        this.metadata = metadata;
    }

}
