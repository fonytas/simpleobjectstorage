package ObjectStorage.model;

import ObjectStorage.service.BucketService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;


@Document
public class Bucket {

    @Autowired
    private BucketService bucketService;

    @Id
    private ObjectId id;
    private String name;
    private String uuid;
    private long modified;
    private long created;
    private Set<ObjectFile> objectFiles;

    public Bucket(String name, String uuid){
        this.name = name;
        this.uuid = uuid;
        this.objectFiles = new HashSet<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public long getModified() {
        return modified;
    }

    public void setModified(long modified) {
        this.modified = modified;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public Set<ObjectFile> getObjectFiles() {
        return objectFiles;
    }

    public void setObjectFiles(Set<ObjectFile> objectFiles) {
        this.objectFiles = objectFiles;
    }




}
