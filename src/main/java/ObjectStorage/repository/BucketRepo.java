package ObjectStorage.repository;

import ObjectStorage.model.Bucket;
import ObjectStorage.model.ObjectFile;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.ArrayList;

public interface BucketRepo extends MongoRepository<Bucket, Integer> {

    Bucket findOneByName(String bucketname);

    void deleteBucketByName(String bucketname);










}
