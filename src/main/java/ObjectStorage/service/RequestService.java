package ObjectStorage.service;

import ObjectStorage.model.Bucket;
import ObjectStorage.model.ObjectFile;
import ObjectStorage.repository.BucketRepo;
import com.google.common.hash.Hasher;
import com.google.common.hash.Hashing;
import com.google.common.io.BaseEncoding;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import static java.lang.Long.valueOf;

@Service("requestService")
public class RequestService {

    @Autowired
    private BucketRepo bucketRepo;

    public boolean canCreate(Bucket bucket, String objectFileName) {

        Set<ObjectFile> list = bucket.getObjectFiles();
        for (ObjectFile s : list) {
            if (s.getObjectFileName().equalsIgnoreCase(objectFileName)){
                return true;
            }
        }
        return false;
    }

    public HashMap createResponse(String contentMD5, Long contentLength, String partNumber) {

        HashMap<String, Object> map = new HashMap<>();
        map.put("length", contentLength);
        map.put("md5", contentMD5);
        map.put("partNumber", partNumber);

        return map;
    }

    public HashMap createErrorResponse(String contentMD5, Long contentLength, String partNumber, String error) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("length", contentLength);
        map.put("md5", contentMD5);
        map.put("partNumber", partNumber);
        map.put("error", error);
        return map;
    }

    public String checkPartNumberValidation(String partNumber) {

        try {
            int number = Integer.parseInt(partNumber);
            if ( number >=1 && number <= 10000) // if number is between 1-10,000
            {
                return "passed";
            }
            else {
                return "InvalidPartNumber";
            }

        }catch (Exception e) {
            return "InvalidPartNumber";

        }

    }

    public boolean isCompleted(Bucket bucket, String objectFileName) {

        Set<ObjectFile> obj = bucket.getObjectFiles();

        for (ObjectFile s : obj) {
            if (s.getObjectFileName().equalsIgnoreCase(objectFileName)) {

                if (s.getTicket().equalsIgnoreCase("completed")) {
                    return true;
                }
            }
        }
        return false;
    }

    //2 - add object part in DB
    private boolean addFilePath(Bucket bucket, String objectFileName, String filePath, String md5, Long contentLength, StringBuffer hexString) {

        Set<ObjectFile> obj = bucket.getObjectFiles();
        ArrayList<Object> list = new ArrayList<>();
        list.add(md5);
        list.add(contentLength);

        if (!md5.equals(hexString.toString())) {
            return false;
        }

        for (ObjectFile s : obj) {
            if (s.getObjectFileName().equalsIgnoreCase(objectFileName)) { // if object exist
                setModified(s, bucket);
                HashMap<Integer, ArrayList> newList = new HashMap<>();
                if (s.getObjectFileParts() == null || s.getObjectFileParts().isEmpty()) { // if there is no part in the object file/
                    newList.put(Integer.parseInt(filePath),list);
                    s.setObjectFileParts(newList);
                    Long totalContentLength = getTotalContentLength(bucket, s.getObjectFileName(), contentLength);
                    s.setTotalContentLength(totalContentLength);
                    bucketRepo.save(bucket);
                }
                else { // if there is something in object file
                    newList = s.getObjectFileParts(); // get the existing parts from DB
                    newList.put(Integer.parseInt(filePath), list); // add new file path
                    s.setObjectFileParts(newList);
                    Long totalContentLength = getTotalContentLength(bucket, s.getObjectFileName(), contentLength);
                    s.setTotalContentLength(totalContentLength);
                    bucketRepo.save(bucket);
                }
            }
        }
        return true;
    }

    private void setModified(ObjectFile objectFile, Bucket bucket) {
        Long time = Instant.now().toEpochMilli();
        bucket.setModified(time);
        objectFile.setModified(time);
    }

    //1
    public boolean saveNewPart(HttpServletRequest request, Bucket bucket,String bucketUUID,
                               String objectFileName,String objectUUID,String part, String contentMD5, Long contentLength) {
        String filePath = "storage/" + bucketUUID + "/" + objectUUID + "/" + objectFileName + "-" + part;

        try {
            FileOutputStream fout = new FileOutputStream(filePath);
            ServletInputStream inputStream = request.getInputStream();
            MessageDigest md = MessageDigest.getInstance("MD5");
            DigestInputStream dis = new DigestInputStream(inputStream, md);
            IOUtils.copy(dis, fout);

            byte[] digest = md.digest();

            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < digest.length; i++) {
                String hex = Integer.toHexString(0xff & digest[i]);
                if (hex.length() == 1)
                    hexString.append('0');
                hexString.append(hex);
            }

            fout.close();
            inputStream.close();
            return addFilePath(bucket, objectFileName, part, contentMD5, contentLength, hexString);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private String calculateMD5Checksum(List<String> md5s) {
        StringBuilder stringBuilder = new StringBuilder();

        for (String md5:md5s) {
            stringBuilder.append(md5);
        }

        String hex = stringBuilder.toString();
        byte raw[] = BaseEncoding.base16().decode(hex.toUpperCase());
        Hasher hasher = Hashing.md5().newHasher();
        hasher.putBytes(raw);
        String digest = hasher.hash().toString();
        return digest + "-" + md5s.size();
    }


    public String getEtag(Bucket bucket, String objectName) {
        Set<ObjectFile> obj = bucket.getObjectFiles();
        HashMap<Integer, ArrayList> allPart;
        List<String> lst = new ArrayList<>();

        for (ObjectFile s : obj) {
            if (s.getObjectFileName().equalsIgnoreCase(objectName)) { // if object exists

                allPart = s.getObjectFileParts();
                for (ArrayList val : allPart.values()) {
                    lst.add(String.valueOf(val.get(0)));
                }
                s.setTicket("completed");
                bucketRepo.save(bucket);
                break;
            }
        }
        return calculateMD5Checksum(lst);
    }

    public HashMap createCompleteResponse(String eTag, Long contentLength, String objectName) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("eTag", eTag);
        map.put("length", contentLength);
        map.put("name", objectName);
        return map;
    }

    public HashMap createCompleteErrorResponse(String eTag, Long contentLength, String objectName, String error) {
        HashMap<String, Object> map = new HashMap<>();
        map.put("eTag", eTag);
        map.put("length", contentLength);
        map.put("name", objectName);
        map.put("error", error);
        return map;
    }

    public Long getTotalContentLength(Bucket bucket, String objectName, Long ct) {

        Set<ObjectFile> obj = bucket.getObjectFiles();
        Long total = 0L;
        for (ObjectFile s : obj) {
            if (s.getObjectFileName().equalsIgnoreCase(objectName)) {
                for (ArrayList t : s.getObjectFileParts().values()) {
                    total = total + (long)t.get(1);
                }
                return total;
            }
        }
        return valueOf(0);
    }
}
