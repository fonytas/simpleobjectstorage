package ObjectStorage.service;


import ObjectStorage.model.Bucket;
import ObjectStorage.model.ObjectFile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.*;


@Service("downloadService")
public class DownloadService {

    public ResponseEntity sendDownload(HttpServletResponse response, Bucket bucket, String range, ObjectFile objectFile) {


        ArrayList<Long> rangeList = getRange(range, objectFile.getTotalContentLength());
        Long start = rangeList.get(0);
        Long end = rangeList.get(1);

        if ( end > objectFile.getTotalContentLength()) {
            end = objectFile.getTotalContentLength();
        }

        try {
            List<InputStream> lst_is = getFilePart(objectFile.getObjectFileParts(), bucket.getUuid(), objectFile.getObjectUUID(), objectFile.getObjectFileName());
            SequenceInputStream sin = new SequenceInputStream(Collections.enumeration(lst_is));
            OutputStream os = new BufferedOutputStream(response.getOutputStream());

            sin.skip(start);
            for (Long counter = 0L; counter <= end-start; counter ++ ) {
                os.write(sin.read());
            }
            os.close();
            sin.close();

            return ResponseEntity.ok("");
        } catch (Exception e) {
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }


    private ArrayList<Long> getRange(String range, Long totalContentLength) {

        ArrayList<Long> c = new ArrayList<>();
        String[] a = range.split("-");
        String[] b = a[0].split("=");

        if (a.length == 1) {
            c.add(Long.valueOf(b[1]));
            c.add(totalContentLength);
            return c;
        }
        c.add(Long.valueOf(b[1]));
        c.add(Long.valueOf(a[1]));
        return c;
    }

    private List<InputStream> getFilePart(HashMap<Integer, ArrayList> map, String bucketUUID, String objectUUID, String objectName) {

        List<InputStream> lst_input = new ArrayList<>();

        for (Integer key : map.keySet()) {
            try {
                FileInputStream fin = new FileInputStream("storage/"+bucketUUID+"/"+objectUUID+"/"+objectName+"-"+key);
                lst_input.add(fin);

            }catch (Exception e){
            }
        }
        return lst_input;
    }
}
