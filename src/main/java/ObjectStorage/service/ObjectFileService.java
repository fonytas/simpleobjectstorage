package ObjectStorage.service;


import ObjectStorage.model.Bucket;
import ObjectStorage.model.ObjectFile;
import org.springframework.stereotype.Service;

import java.io.File;
import java.time.Instant;
import java.util.Set;

@Service("objectFileService")
public class ObjectFileService {

    public void setModified(ObjectFile objectFile) {
        Long time = Instant.now().toEpochMilli();
        objectFile.setModified(time);
    }

    public void setCreated(ObjectFile objectFile) {
        Long time = Instant.now().toEpochMilli();
        objectFile.setCreated(time);
    }

    public boolean createObjectDirectory(ObjectFile objectFile, Bucket bucket) {
        File file;

        try {
            // returns pathnames for files and directory
            file = new File("storage/"+bucket.getUuid()+"/"+objectFile.getObjectUUID());

            // create
            return file.mkdir();

        } catch(Exception e) {
            // if any error occurs
            e.printStackTrace();
        }
        return false;
    }

    public String getObjectUUID(Bucket bucket, String objectFileName){

        Set<ObjectFile> newObject = bucket.getObjectFiles();
        for (ObjectFile s : newObject) {
            if (s.getObjectFileName().equalsIgnoreCase(objectFileName)){
                return s.getObjectUUID();
            }
        }
        return "";

    }

    public boolean isObjectFilenNameValid(String name) {
        String specialChar = ".-_";
        return specialChar.indexOf(name.charAt(0)) < 0 && specialChar.indexOf(name.charAt(name.length() - 1)) < 0;
    }

    public boolean isObjectNameExist(Bucket bucket, String objectName) {
        Set<ObjectFile> obj = bucket.getObjectFiles();

        for (ObjectFile s : obj) {
            if (s.getObjectFileName().equalsIgnoreCase(objectName)) { // if object exists
                return true;
            }
        }
        return false;
    }
}
