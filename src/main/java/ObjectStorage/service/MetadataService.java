package ObjectStorage.service;


import ObjectStorage.model.Bucket;
import ObjectStorage.model.ObjectFile;
import ObjectStorage.repository.BucketRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Service("metadataService")
public class MetadataService {

    @Autowired
    BucketRepo bucketRepo;

    public ResponseEntity addMetadata(Bucket bucket, String objectname, String key, String data) {

        Set<ObjectFile> objectList = bucket.getObjectFiles();
        HashMap<String, String> newMap;

        for (ObjectFile obj : objectList) {

            if (obj.getObjectFileName().equalsIgnoreCase(objectname)) { // if object exist
                newMap = obj.getMetadata();
                newMap.put(key, data);
                bucketRepo.save(bucket);
                return ResponseEntity.ok("");

            }
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    public ResponseEntity deleteMetadata(Bucket bucket, String objectname, String key) {

        Set<ObjectFile> objectList = bucket.getObjectFiles();
        HashMap<String, String> newMap;

        for (ObjectFile obj : objectList) {

            if (obj.getObjectFileName().equalsIgnoreCase(objectname)) { // if object exist
                newMap = obj.getMetadata();

//                if (newMap.containsKey(key)) {

                newMap.remove(key);
                bucketRepo.save(bucket);
                return ResponseEntity.ok("");
//                }
            }
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    public ResponseEntity getMetadata(Bucket bucket, String objectname, String key) {

        Set<ObjectFile> objectList = bucket.getObjectFiles();
        HashMap<String, String> newMap;
        HashMap<String, String> metadata = new HashMap<>();

        for (ObjectFile obj : objectList) {

            if (obj.getObjectFileName().equalsIgnoreCase(objectname)) { // if object exist
                newMap = obj.getMetadata();
                for (Map.Entry<String,String> entry : newMap.entrySet()) {


                    if (entry.getKey().equalsIgnoreCase(key.replace("\"", ""))) {
                        String value = entry.getValue();
                        String newKey = entry.getKey();

                        metadata.put(newKey, value);
                        return ResponseEntity.ok().body(metadata);
                    }
                    else {
                        return ResponseEntity.ok().body(metadata);

                    }
                }
            }
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    public ResponseEntity getAllMetadata(Bucket bucket, String objectname) {

        Set<ObjectFile> objectList = bucket.getObjectFiles();
        HashMap<String, String> newMap;

        for (ObjectFile obj : objectList) {

            if (obj.getObjectFileName().equalsIgnoreCase(objectname)) { // if object exist
                newMap = obj.getMetadata();
                return ResponseEntity.ok().body(newMap);
            }
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
