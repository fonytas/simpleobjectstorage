package ObjectStorage.service;

import ObjectStorage.model.Bucket;
import ObjectStorage.model.ObjectFile;
import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.util.*;

@Service("bucketService")
public class BucketService {

    public HashMap<String, Object> getResponse(Bucket bucket) {

        HashMap<String, Object> map = new HashMap<>();

        map.put("name", bucket.getName());
        map.put("modified",bucket.getModified());
        map.put("created", bucket.getCreated());

        return map;
    }

    public boolean createDirectory(Bucket bucket) {

        File file;

        try {
            // returns pathnames for files and directory
            file = new File("storage/"+bucket.getUuid());

            // create
            return file.mkdir();

        } catch(Exception e) {
            // if any error occurs
            e.printStackTrace();

        }
        return false;

    }


    public HashMap<String, Object> getBucketInfo(Bucket bucket) {

        HashMap<String, Object> bucketInfo = new HashMap<>();

        Set<HashMap> newList = new HashSet<>();

        Set<ObjectFile> list = bucket.getObjectFiles();

        for (ObjectFile s : list) {

            HashMap<String , Object> map = new HashMap<>();
            map.put("name", s.getObjectFileName());
            map.put("created", s.getCreated());
            map.put("modified", s.getModified());

            newList.add(map);
        }

        bucketInfo.put("name", bucket.getName());
        bucketInfo.put("modified", bucket.getModified());
        bucketInfo.put("created", bucket.getCreated());
        bucketInfo.put("objects", newList);

        return bucketInfo;
    }

    public String RandomStringUUID() {
            // Creating a random UUID (Universally unique identifier).
            UUID uuid = UUID.randomUUID();
            return uuid.toString();
    }

    public void setModified(Bucket bucket) {
        Long time = Instant.now().toEpochMilli();
        bucket.setModified(time);

    }

    public void setCreated(Bucket bucket) {
        Long time = Instant.now().toEpochMilli();
        bucket.setCreated(time);
    }

    public boolean isBucketNameValid(String name) {
        String specialChar = ".";
        return specialChar.indexOf(name.charAt(0)) < 0 && specialChar.indexOf(name.charAt(name.length() - 1)) < 0;
    }




}
