package ObjectStorage.controller;


import ObjectStorage.model.Bucket;
import ObjectStorage.repository.BucketRepo;
import ObjectStorage.service.BucketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

@RestController
public class BucketController {

    @Autowired
    private BucketRepo bucketRepo;

    @Autowired
    private BucketService bucketService;

    @PostMapping(value = "/{bucketname}", params = "create")
    public ResponseEntity createBucket(@PathVariable(name = "bucketname") String bucketname) {

        if (!bucketService.isBucketNameValid(bucketname.toLowerCase())) { // if name start or end with .
            return ResponseEntity.badRequest().body("");
        }

        Bucket bucket = new Bucket(bucketname.toLowerCase(), bucketService.RandomStringUUID());

        if (bucketRepo.findOneByName(bucketname.toLowerCase()) == null) { // if the bucket does not already exist, create one
            bucketService.setCreated(bucket);
            bucketService.setModified(bucket);
            bucketService.createDirectory(bucket);
            bucketRepo.insert(bucket);
            HashMap<String, Object> responseStatus = bucketService.getResponse(bucket);
            return  ResponseEntity.ok().body(responseStatus);
        }
        else {
            return ResponseEntity.badRequest().body("");
        }
    }
    @DeleteMapping(value = "/{bucketname}", params = "delete")
    public ResponseEntity deleteBucket(@PathVariable(name = "bucketname") String bucketname) throws IOException {

        if (bucketRepo.findOneByName(bucketname.toLowerCase()) != null) { // if the bucket can be deleted
            bucketRepo.deleteBucketByName(bucketname.toLowerCase());
            return ResponseEntity.ok("200");
        }
        else {
            return ResponseEntity.badRequest().body("400");
        }
    }

    @GetMapping("/all")
    public List<Bucket> getAll() {
        return bucketRepo.findAll();
    }

    @GetMapping(value = "/{bucketname}", params = "list")
    public ResponseEntity listAll(@PathVariable(name = "bucketname") String bucketname) {

        if (bucketRepo.findOneByName(bucketname.toLowerCase()) != null) { // if the bucket exists
            return ResponseEntity.ok().body(bucketService.getBucketInfo(bucketRepo.findOneByName(bucketname)));
        }
        return ResponseEntity.badRequest().body("400");
    }
}