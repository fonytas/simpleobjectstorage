package ObjectStorage.controller;


import ObjectStorage.model.Bucket;
import ObjectStorage.repository.BucketRepo;
import ObjectStorage.service.MetadataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
public class MetadataController {

    @Autowired
    BucketRepo bucketRepo;

    @Autowired
    MetadataService metadataService;

    @PutMapping(value = "/{bucketname}/{objectname}", params = "metadata")
    public ResponseEntity addAndUpdateMetadata(@PathVariable (name = "bucketname") String bucketname,
                                               @PathVariable (name = "objectname") String objectname,
                                               @RequestParam (value = "key") String key,
                                               @RequestBody String data) {

        Bucket bucket = bucketRepo.findOneByName(bucketname.toLowerCase());

        if ( bucket != null ) { // if bucket exist
            return metadataService.addMetadata(bucket, objectname, key, data);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping(value = "/{bucketname}/{objectname}", params = "metadata")
    public ResponseEntity deleteMetadata(@PathVariable (name = "bucketname") String bucketname,
                                         @PathVariable (name = "objectname") String objectname,
                                         @RequestParam (value = "key") String key) {
        Bucket bucket = bucketRepo.findOneByName(bucketname.toLowerCase());

        if ( bucket != null ) { // if bucket exist
            return metadataService.deleteMetadata(bucket, objectname, key);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping(value = "/{bucketname}/{objectname}", params = "metadata")
    public ResponseEntity getAllMetadata(@PathVariable (name = "bucketname") String bucketname,
                                           @PathVariable (name = "objectname") String objectname,
                                           @RequestParam (value = "key", required = false) String key) {
        Bucket bucket = bucketRepo.findOneByName(bucketname.toLowerCase());
        if ( bucket != null ) {
            if (key == null) {
                return metadataService.getAllMetadata(bucket, objectname);
            }
            return metadataService.getMetadata(bucket, objectname, key);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}

