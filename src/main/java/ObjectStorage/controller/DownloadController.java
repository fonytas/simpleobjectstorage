package ObjectStorage.controller;


import ObjectStorage.model.Bucket;
import ObjectStorage.model.ObjectFile;
import ObjectStorage.repository.BucketRepo;
import ObjectStorage.service.DownloadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletResponse;
import java.util.Set;

@RestController
public class DownloadController {

    @Autowired
    BucketRepo bucketRepo;

    @Autowired
    DownloadService downloadService;

    @GetMapping(value = "/{bucketname}/{objectname}")
    public ResponseEntity downloadObject(HttpServletResponse response,
                                         @PathVariable( name =  "bucketname") String bucketname,
                                         @PathVariable( name = "objectname") String objectname,
                                         @RequestHeader( name = "Range", required = false) String range) {

        Bucket bucket = bucketRepo.findOneByName(bucketname.toLowerCase());

        if ( bucket != null ) {

            Set<ObjectFile> objectList = bucket.getObjectFiles();
            for (ObjectFile obj : objectList) {

                if (obj.getObjectFileName().equalsIgnoreCase(objectname)) { // if object exist

                    if (obj.getTicket().equalsIgnoreCase("completed")) {
                        if (range == null) {
                            range = "bytes=0-" + String.valueOf(obj.getTotalContentLength()-1);
                        }
                        return downloadService.sendDownload(response, bucket, range, obj);
                    }
                    else {
                        return ResponseEntity.badRequest().body("");
                    }
                }
            }
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
