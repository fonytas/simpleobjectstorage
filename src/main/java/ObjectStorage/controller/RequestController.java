package ObjectStorage.controller;

import ObjectStorage.model.Bucket;
import ObjectStorage.repository.BucketRepo;
import ObjectStorage.service.ObjectFileService;
import ObjectStorage.service.RequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import java.io.*;

import static java.lang.Long.valueOf;

@Controller
public class RequestController {

    @Autowired
    private BucketRepo bucketRepo;

    @Autowired
    private RequestService requestService;

    @Autowired
    private ObjectFileService objectFileService;

    @PutMapping(value = "/{bucketname}/{objectname}", params = "partNumber")
    public ResponseEntity getUploadRequest(HttpServletRequest request,
                                           @RequestParam(value = "partNumber") String partNumber,
                                           @PathVariable(name = "bucketname") String bucketname,
                                           @PathVariable(name = "objectname") String objectname,
                                           @RequestHeader(name="content-md5") String contentMD5,
                                           @RequestHeader(name= "content-length") Long contentLength) throws IOException {
        String error;
        Bucket bucket = bucketRepo.findOneByName(bucketname.toLowerCase());

        if (bucket != null) {

            if (!requestService.checkPartNumberValidation(partNumber).equalsIgnoreCase("passed")) {
                error = "InvalidPartNumber";
                return ResponseEntity.badRequest().body(requestService.createErrorResponse(contentMD5, contentLength, partNumber, error));
            }
            if (requestService.canCreate(bucket, objectname)) {
                String bucketUUID = bucket.getUuid();
                String objectUUID = objectFileService.getObjectUUID(bucket, objectname);

                if (requestService.isCompleted(bucket, objectname)) {
                    error = "Flag is completed";
                    return ResponseEntity.badRequest().body(requestService.createErrorResponse(contentMD5, contentLength, partNumber, error));
                }
                else if (requestService.saveNewPart(request, bucket,bucketUUID, objectname,objectUUID,partNumber, contentMD5, contentLength)){
                    return ResponseEntity.ok().body(requestService.createResponse(contentMD5, contentLength, partNumber));
                }
                else {
                    error = "MD5Mismatched";
                    return ResponseEntity.badRequest().body(requestService.createErrorResponse(contentMD5, contentLength, partNumber, error));
                }
            }
            else {
                error = "InvalidObjectName";
                return ResponseEntity.badRequest().body(requestService.createErrorResponse(contentMD5, contentLength, partNumber, error));
            }
        }
        else {
            error = "InvalidBucket";
            return ResponseEntity.badRequest().body(requestService.createErrorResponse(contentMD5, contentLength, partNumber, error));
        }
    }

    @PostMapping(value = "/{bucketname}/{objectname}", params = "complete")
    public ResponseEntity completeMultipartUpload(@PathVariable(name = "bucketname") String bucketname,
                                                  @PathVariable(name = "objectname") String objectname)  {

        Bucket bucket = bucketRepo.findOneByName(bucketname.toLowerCase());
        String eTag;
        String error;

        if (bucket != null) { // if bucket exists

            Long totalContentLength = requestService.getTotalContentLength(bucket, objectname, 0L );

            if (objectFileService.isObjectNameExist(bucket, objectname)) { // if object exist in bucket
                eTag = requestService.getEtag(bucket, objectname);
                return ResponseEntity.ok().body(requestService.createCompleteResponse(eTag, totalContentLength, objectname));
            }
            return ResponseEntity.badRequest().body(requestService.createCompleteErrorResponse("", valueOf(0), objectname,"InvalidObjectName"));  // if object does not exist
        } else {
            error = "InvalidBucket";
            return ResponseEntity.badRequest().body(requestService.createCompleteErrorResponse("", valueOf(0), objectname,error));  // if object does not exist
        }
    }
}


