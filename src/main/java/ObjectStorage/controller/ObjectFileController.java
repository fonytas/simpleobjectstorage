package ObjectStorage.controller;


import ObjectStorage.model.Bucket;
import ObjectStorage.model.ObjectFile;
import ObjectStorage.repository.BucketRepo;
import ObjectStorage.repository.ObjectFileRepo;
import ObjectStorage.service.BucketService;
import ObjectStorage.service.ObjectFileService;
import ObjectStorage.service.RequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

@RestController
public class ObjectFileController {

    @Autowired
    private BucketRepo bucketRepo;

    @Autowired
    private BucketService bucketService;

    @Autowired
    private ObjectFileService objectFileService;

    @Autowired
    private RequestService requestService;

    @PostMapping(value = "/{bucketname}/{objectname}", params = "create")
    public ResponseEntity createObjectFile(@PathVariable(name = "bucketname") String bucketname,
                                           @PathVariable(name = "objectname") String objectname) {

        if (!objectFileService.isObjectFilenNameValid(objectname.toLowerCase())) { // if the object file name start or end with - _ .
            return ResponseEntity.badRequest().body("400");
        }

        Bucket bucket = bucketRepo.findOneByName(bucketname.toLowerCase());

        if (bucket != null) { // if bucket exists
            ObjectFile newObjectFile = new ObjectFile(objectname, bucketService.RandomStringUUID());
            objectFileService.setModified(newObjectFile);
            objectFileService.setCreated(newObjectFile);

            if (bucket.getObjectFiles() == null || bucket.getObjectFiles().isEmpty()) {  // if there is no object in the bucket
                Set<ObjectFile> newObject = new HashSet<>();
                objectFileService.createObjectDirectory(newObjectFile, bucket);
                newObject.add(newObjectFile);
                bucket.setObjectFiles(newObject);
                bucketRepo.save(bucket);
                return ResponseEntity.ok("");

            }
            else { // if the bucket already has objects

                // check if object already exist
                Set<ObjectFile> list = bucket.getObjectFiles();

                for (ObjectFile s : list) {
                    if (objectname.equalsIgnoreCase(s.getObjectFileName())) {   // if the object already exist...
                        return ResponseEntity.badRequest().body("400");
                    }
                }
                objectFileService.createObjectDirectory(newObjectFile, bucket);
                list.add(newObjectFile);
                bucket.setObjectFiles(list);
                bucketRepo.save(bucket);
                return ResponseEntity.ok("");

            }
        }
        return ResponseEntity.badRequest().body("400");
    }

    @DeleteMapping(value = "/{bucketname}/{objectname}", params = "partNumber")
    public ResponseEntity deletePart(@PathVariable(name = "bucketname") String bucketname,
                                     @PathVariable(name = "objectname") String objectname,
                                     @RequestParam(name = "partNumber") Integer partNumber) {

        Bucket bucket = bucketRepo.findOneByName(bucketname.toLowerCase());

        // check if bucket exist
        if (bucket != null) {
            Set<ObjectFile> objectList = bucket.getObjectFiles();
            HashMap<Integer , ArrayList> objectPartList;
            Long totalContentLength;

            for (ObjectFile obj : objectList) {
                if (obj.getObjectFileName().equalsIgnoreCase(objectname)) { // check if object exist
                    if (!obj.getTicket().equalsIgnoreCase("completed")) { // check if the object has 'completed' flag
                        if (requestService.checkPartNumberValidation(partNumber.toString()).equalsIgnoreCase("passed")) { // if partnumber is valid
                            objectPartList = obj.getObjectFileParts();
                            if (objectPartList.containsKey(partNumber)) {

                                totalContentLength = obj.getTotalContentLength();
                                totalContentLength -= (Long) objectPartList.get(partNumber).get(1);
                                obj.setTotalContentLength(totalContentLength);
                                objectPartList.remove(partNumber);

                                objectFileService.setModified(obj);
                                bucketService.setModified(bucket);

                                bucketRepo.save(bucket);
                                return ResponseEntity.ok("");
                            }
                            else {
                                return ResponseEntity.badRequest().body("");
                            }
                        }

                    }
                }
            }
            return ResponseEntity.badRequest().body("");
        }
        else { // if bucket is empty
            return ResponseEntity.badRequest().body("");
        }
    }

    @DeleteMapping(value = "/{bucketname}/{objectname}", params = "delete")
    public ResponseEntity deleteObject(@PathVariable(name = "bucketname") String bucketname,
                                     @PathVariable(name = "objectname") String objectname) {

        Bucket bucket = bucketRepo.findOneByName(bucketname.toLowerCase());

        // check if bucket exist
        if (bucket != null) {
            Set<ObjectFile> objectList = bucket.getObjectFiles();

            for (ObjectFile obj : objectList) {
                if (obj.getObjectFileName().equalsIgnoreCase(objectname)) { // check if object exist
                    objectList.remove(obj);
                    bucket.setObjectFiles(objectList);
                    bucketService.setModified(bucket);
                    bucketRepo.save(bucket);
                    return ResponseEntity.ok("");
                }
            }
            return ResponseEntity.badRequest().body("");
        }
        else { // if bucket is empty
            return ResponseEntity.badRequest().body("");
        }
    }
}







