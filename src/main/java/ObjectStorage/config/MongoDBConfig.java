package ObjectStorage.config;

import ObjectStorage.repository.BucketRepo;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;


@EnableMongoRepositories(basePackageClasses = BucketRepo.class )
@Configuration
public class MongoDBConfig {


}
