import requests
import hashlib

BASE_URL = 'http://localhost:8080'
STATUS_OK = requests.codes['ok']
STATUS_BAD = 400
STATUS_NOTFOUND = 404



def test_create_bucket1():
    resp = requests.post(BASE_URL + '/testapi?create')
    assert resp.status_code == STATUS_OK

    resp = requests.post(BASE_URL + '/.testapi?create')
    assert resp.status_code == STATUS_BAD

    resp = requests.post(BASE_URL + '/testapi.?create')
    assert resp.status_code == STATUS_BAD

    resp = requests.post(BASE_URL + '/testapi2?create')
    assert resp.status_code == STATUS_OK

    resp = requests.post(BASE_URL + '/testapi3?create')
    assert resp.status_code == STATUS_OK
   


def test_drop_bucket():

  resp = requests.delete(BASE_URL + '/testapi3?delete')
  assert resp.status_code == STATUS_OK


def test_list_objects():

  resp = requests.get(BASE_URL + '/testapi?list')
  assert resp.status_code == STATUS_OK


def test_upload():

    resp = requests.post(BASE_URL + '/testapi/fonytas?create')
    assert resp.status_code == STATUS_OK

    resp = requests.post(BASE_URL + '/testapi/fonytas2?create')
    assert resp.status_code == STATUS_OK

    data = open('./elmo.jpg', 'rb').read()
    headers={'Content-Length': str(len(data)), 'Content-MD5': hashlib.md5(data).hexdigest()}
    resp = requests.put(url=BASE_URL+'/testapi/fonytas2?partNumber=1',data=data,headers=headers) 
    assert resp.status_code == STATUS_OK

    data = open('./first.txt', 'rb').read()
    headers={'Content-Length': str(len(data)), 'Content-MD5': hashlib.md5(data).hexdigest()}
    resp = requests.put(url=BASE_URL+'/testapi/fonytas?partNumber=1',data=data,headers=headers) 
    assert resp.status_code == STATUS_OK

    data = open('./second.txt', 'rb').read()
    headers={'Content-Length': str(len(data)), 'Content-MD5': hashlib.md5(data).hexdigest()}
    resp = requests.put(url=BASE_URL+'/testapi/fonytas?partNumber=2',data=data,headers=headers) 
    assert resp.status_code == STATUS_OK


    data = open('./third.txt', 'rb').read()
    headers={'Content-Length': str(len(data)), 'Content-MD5': hashlib.md5(data).hexdigest()}
    resp = requests.put(url=BASE_URL+'/testapi/fonytas?partNumber=3',data=data,headers=headers) 
    assert resp.status_code == STATUS_OK

    data = open('./elmo.jpg', 'rb').read()
    headers={'Content-Length': str(len(data)), 'Content-MD5': hashlib.md5(data).hexdigest()}
    resp = requests.put(url=BASE_URL+'/testapi/fonytas?partNumber=4',data=data,headers=headers) 
    assert resp.status_code == STATUS_OK

    data = open('./second.txt', 'rb').read()
    headers={'Content-Length': str(len(data)), 'Content-MD5': hashlib.md5(data).hexdigest()}
    resp = requests.put(url=BASE_URL+'/testapifake/fonytasfake?partNumber=2',data=data,headers=headers) 
    assert resp.status_code == STATUS_BAD



def test_delete_part():

    resp = requests.delete(BASE_URL+'/testapi/fonytas?partNumber=4')
    assert resp.status_code == STATUS_OK

def test_delete_object():

    resp =requests.delete(BASE_URL+'/testapi/fonytasfake?delete')
    assert resp.status_code == STATUS_BAD

 

def test_complete():

    resp = requests.post(BASE_URL + '/testapifake/fonytas?complete')
    assert resp.status_code == STATUS_BAD

    resp = requests.post(BASE_URL + '/testapi/fonytaseiei?complete')
    assert resp.status_code == STATUS_BAD

    resp = requests.post(BASE_URL + '/testapi/fonytas?complete')
    assert resp.status_code == STATUS_OK

    data = open('./third.txt', 'rb').read()
    headers={'Content-Length': str(len(data)), 'Content-MD5': hashlib.md5(data).hexdigest()}
    resp = requests.put(url=BASE_URL+'/testapi/fonytas?partNumber=4',data=data,headers=headers) 
    assert resp.status_code == STATUS_BAD

    resp = requests.delete(BASE_URL+'/testapi/fonytas?partNumber=3')
    assert resp.status_code == STATUS_BAD

def test_metadata():

    resp = requests.put(BASE_URL+'/testapi/fonytas?metadata&key=dataSource', 
                            data="http://www.ietf.org/rfc/rfc2616.txt")
    assert resp.status_code == STATUS_OK

    resp = requests.put(BASE_URL+'/testapi/fonytas?metadata&key=license', 
                            data="Apache 2.0")
    assert resp.status_code == STATUS_OK

    resp = requests.put(BASE_URL+'/testapi/fonytas?metadata&key=license', 
                            data="Apache 2.1")
    assert resp.status_code == STATUS_OK


    resp = requests.put(BASE_URL+'/testapi/fonytas?metadata&key=fonytas', 
                            data="testapi")
    assert resp.status_code == STATUS_OK


    resp = requests.put(BASE_URL+'/buckettest/elmo?metadata&key=dataSource', 
                            data="http://www.ietf.org/rfc/rfc2616.txt")
    assert resp.status_code == STATUS_NOTFOUND


    resp = requests.delete(BASE_URL+'/testapi/fonytas?metadata&key=fonytas')
    assert resp.status_code == STATUS_OK

    resp = requests.get(BASE_URL+'/testapi/fonytas?metadata')
    assert resp.status_code == STATUS_OK


    resp = requests.get(BASE_URL+'/testapi/fonytas?metadata&key=dataSource')
    assert resp.status_code == STATUS_OK




















