FROM openjdk:8
WORKDIR /app
ADD target/SimpleObjectStorage.jar simpleobjectstorage.jar
ADD src/main/resources/application.properties application.properties
EXPOSE 8080
#ARG JAR_FILE
#COPY ${JAR_FILE} simpleobjectstorage.jar
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom", "-jar", "simpleobjectstorage.jar"]